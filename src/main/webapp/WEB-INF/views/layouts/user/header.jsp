	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ include file="/WEB-INF/views/layouts/user/taglib.jsp" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<header id="header">
			<div class="row">
				<div class="span4">
					<h3>
						<a class="logo" href="<c:url value="/trang-chu"/>"><span>Trang sức thời trang</span> 
						<img src="<c:url	value="/assets/user/img/logo-bootstrap-shoping-cart.png"/>"	
							alt="bootstrap sexy shop"> </a>
					</h3>
				</div>
				<div class="span4">
					<div class="offerNoteWrapper">
						<h3 class="dotmark">
							<i class="icon-cut"></i> Ngày hội mua sắm trang sức thời trang !
						</h3>
					</div>
				</div>
				<div class="span4 alignR">
					<p>
						<br> <strong> Support (24/7) : 0909 766 334 </strong><br>
						<br>
					</p>
					<span class="btn btn-mini"><a href="<c:url value="/gio-hang"/>">[${TotalQuantyCart }] </a><span
						class="icon-shopping-cart"></span></span> <span
						class="btn btn-warning btn-mini">$</span> <span
						class="btn btn-mini">&pound;</span> <span class="btn btn-mini">&euro;</span>
				</div>
			</div>
		</header>

		<!--
Navigation Bar Section 
-->
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<a data-target=".nav-collapse" data-toggle="collapse"
						class="btn btn-navbar"> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
					</a>
					<div class="nav-collapse">
						<ul class="nav">
							<c:forEach var="item" items="${menus }" varStatus="index">
								<c:if test="${index.first }">
									<li class="active">
								</c:if>
									
								<c:if test="${not index.first }">
									<li class="">
								</c:if>
									<a href="<c:url value="${item.url}"/>">${item.name } </a></li>
								
							</c:forEach>
							
						</ul>
						
						<form:form action="search" class="navbar-search pull-left" method="POST" modelAttribute="productsDto">
							<form:input type="text" placeholder="Search" path="name" class="search-query span2" />
							 <form:button type="submit"><i class="fa fa-search"></i></form:button> 
						</form:form>
						
						
						<ul class="nav pull-right">
						<c:if test="${empty LoginInfo }">
							<li class="dropdown"><a data-toggle="dropdown"
								class="dropdown-toggle" href="<c:url value="/dang-ky"/>"><span class="icon-lock"></span>
									Đăng nhập / Đăng ký <b class="caret"></b></a>
								<div class="dropdown-menu">
									<form class="form-horizontal loginFrm">
										<div class="control-group">
											<input type="text" class="span2" id="inputEmail"
												placeholder="Email">
										</div>
										<div class="control-group">
											<input type="password" class="span2" id="inputPassword"
												placeholder="Password">
										</div>
										<div class="control-group">
											<label class="checkbox"> <input type="checkbox">
												Remember me
											</label>
											<button type="submit" class="shopBtn btn-block">Sign
												in</button>
										</div>																																														
									</form>
								</div></li>
							</c:if>
							<c:if test="${not empty LoginInfo}">
								<li><a href="#">Khách hàng ${LoginInfo.display_name  }</a></li>
							</c:if>
						</ul>
					</div>
				</div>
			</div>
		</div>