<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ include file="/WEB-INF/views/layouts/user/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="utf-8">
<title><decorator:title default="Master-layout"/></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Bootstrap styles -->
<link href="<c:url value ="/assets/user/css/bootstrap.css"/>" rel="stylesheet" />
<!-- Customize styles -->
<link <c:url value="/assets/user/style.css"/> rel="stylesheet" />
<!-- font awesome styles -->
<link href="<c:url value ="/assets/user/font-awesome/css/font-awesome.css" />" rel="stylesheet">
<!--[if IE 7]>
			<link href="css/font-awesome-ie7.min.css" rel="stylesheet">
		<![endif]-->

<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

<!-- Favicons -->
<link rel="shortcut icon" <c:url  value="assets/user/ico/favicon.ico"/> />
</head>
<body>
	<!-- 
	Upper Header Section 
-->
	<div class="navbar navbar-inverse navbar-fixed-top" style="text-align: right;">
		<div class="topNav">
			<div class="container">
				<div class="alignR">
					<div class="pull-left socialNw">
						<a href="#"><span class="icon-twitter"></span></a> <a href="#"><span
							class="icon-facebook"></span></a> <a href="#"><span
							class="icon-youtube"></span></a> <a href="#"><span
							class="icon-tumblr"></span></a>
					</div>
					<a class="active" href="<c:url value="/trang-chu"/>"> <span class="icon-home"></span>
						Trang chủ
					</a> 
					<c:if test="${not empty LoginInfo }">
					<a href="#"><span class="icon-user"></span>Xin chào ${LoginInfo.display_name}</a> 
					<a href="<c:url value="/dang-xuat"/>"><span class="icon-edit"></span>Đăng xuất</a> 
					</c:if>
					<c:if test="${empty LoginInfo }">	
					<a href="<c:url value="/dang-ky"/>"><span class="icon-envelope"></span>Đăng ký</a>
					</c:if>
					<a href ="<c:url value="/lien-he"/>"><span class="icon-envelope"></span>
						Liên hệ</a> <a href="<c:url value="/gio-hang"/>"> <span
						class="icon-shopping-cart"></span> ${TotalQuantyCart} Sản Phẩm 	 	 - <span
						class="badge badge-warning"><span class="pull-right"><fmt:formatNumber type = "number" 
         maxFractionDigits = "3" value = "${TotalPriceCart }" />đ</span> </span></a>
				</div>
			</div>
		</div>
	</div>

	<!--
Lower Header Section 
-->
	<div class="container">
		<div id="gototop"></div>
		
		<!-- 
Body Section 
-->

		<%@ include file="/WEB-INF/views/layouts/user/header.jsp" %>
		<decorator:body/>
		
		 
		<!-- 
Clients 
-->
		<section class="our_client">
			<hr class="soften" />
			<h4 class="title cntr">
				<span class="text">Manufactures</span>
			</h4>
			<hr class="soften" />
			<div class="row">
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/1.png"/>"></a>
				</div>
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/2.png"/>"></a>
				</div>
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/3.png"/>"></a>
				</div>
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/4.png"/>"></a>
				</div>
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/5.png"/>"></a>
				</div>
				<div class="span2">
					<a href="#"><img alt="" src="<c:url value ="/assets/user/img/6.png"/>"></a>
				</div>
			</div>
		</section>

		<!--
Footer
-->
		
	</div>
	<!-- /container -->


	
	
	<div class="copyright">
		<div class="container">
			<p class="pull-right">
				<a href="#"><img src="<c:url value="/assets/user/img/maestro.png"/>" alt="payment"></a>
				<a href="#"><img src="<c:url value="/assets/user/img/mc.png"/>" alt="payment"></a> 
				<a href="#"><img src="<c:url value="/assets/user/img/pp.png"/>" alt="payment"></a> 
				<a href="#"><img src="<c:url value="/assets/user/img/visa.png"/>" alt="payment"></a> 
				<a href="#"><img src="<c:url value="/assets/user/img/disc.png"/>" alt="payment"></a>
				
					<!-- footer -->
	<%@ include file="/WEB-INF/views/layouts/user/footer.jsp" %>
			</p>
			<p class="text-center">Copyright &copy; 2021<br> Bảo Toàn's product: Diamond-Shop
			</p>
		</div>
	</div>
	

	
	
	<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
	<!-- Placed at the end of the document so the pages load faster -->
	<script <c:url value="https://ajax.googleapis.com/assets/user/js/jquery.js"/>></script>
	<script <c:url value="http://ajax.googleapis.com/assets/user/js/bootstrap.min.js"/>></script>
	<script <c:url value="http://ajax.googleapis.com/assets/user/js/jquery.easing-1.3.min.js"/>></script>
	<script <c:url value="http://ajax.googleapis.com/assets/user/js/jquery.scrollTo-1.4.3.1-min.js"/>></script>
	<script <c:url value="http://ajax.googleapis.com/assets/user/js/shop.js"/>></script>
	<decorator:getProperty property="page.script"></decorator:getProperty>
</body>
</html>
