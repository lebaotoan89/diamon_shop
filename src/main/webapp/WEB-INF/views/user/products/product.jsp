<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layouts/user/taglib.jsp" %>
<!DOCTYPE html SYSTEM "about:legacy-compat">
<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>
</head>
<body>
	<!-- 
Body Section 
-->
<%-- <h1>${Cart.quanty }</h1>
<h1>${Cart.size() }</h1> --%>
	<div class="row">
<div id="sidebar" class="span3">
	<div class="well well-small">
				<ul class="nav nav-list">
				
				<c:forEach var="item" items="${categorys }">
					<li><a href='<c:url value="/san-pham/${item.id }"/>'><span
							class=""></span><i class="icon-circle-blank"><strong> ${item.name}</strong> </i></a></li>
				</c:forEach>
			
					<li style="border: 0">&nbsp;</li>
					<li><a class="totalInCart" href="gio-hang"><strong>Total
								Amount <span class="pull-right"><fmt:formatNumber type = "number" 
        								 maxFractionDigits = "3" value = "${TotalPriceCart }" />đ</span>
						</strong></a></li>
				</ul>
	</div>

			  <div class="well well-small alert alert-warning cntr">
				  <h2>50% Discount</h2>
				  <p> 
					 only valid for online order. <br><br><a class="defaultBtn" href="#">Click here </a>
				  </p>
			  </div>
			 
			

	</div>
	<div class="span9">
    <ul class="breadcrumb">
    <li><a href="<c:url value="/trang-chu"/>">Trang chủ</a> <span class="divider">/</span></li>
    <li><a href="<c:url value="/trang-chu"/>">Sản phẩm</a> <span class="divider">/</span></li>
    <li class="active">Chi tiết sản phẩm</li>
    </ul>	
	<div class="well well-small">
	<div class="row-fluid">
			<div class="span5">
			<div id="myCarousel" class="carousel slide cntr">
                <div class="carousel-inner">
                  <div class="item active">
                   <a href="#"> <img src="<c:url value="/assets/user/img/${product.img }"/>" alt="" style="width:100%"></a>
                  </div>
                  
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
			</div>
			<div class="span7">
				<h3>${product.name }</h3>
				<hr class="soft"/>
				
				<form class="form-horizontal qtyFrm" method="post" action="<c:url value="/AddCart/${product.id_product }"/>"	>
				  <div class="control-group">
					<label class="control-label"><span><fmt:formatNumber type = "number" 
         maxFractionDigits = "3" value = "${product.price }" />đ</span></label>
					<div class="controls">
					<input type="number" class="span6" min="0" placeholder="Qty.">
					</div>
				  </div>
				
				  <div class="control-group">
					<label class="control-label"><span>Color</span></label>
					<div class="controls">
					  <select class="span11">
						  <option>Red</option>
						  <option>Purple</option>
						  <option>Pink</option>
						  <option>Red</option>
						</select>
					</div>
				  </div>
				  
				  <h4></h4>
				  <p>${product.title }
				  <p>
				  <button type="submit" class="shopBtn"><span class=" icon-shopping-cart"></span>Thêm giỏ hàng</button>
				</form>
			</div>
			</div>
				<hr class="softn clr"/>


            <ul id="productDetail" class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab">Chi tiết sản phẩm</a></li>
              <li class=""><a href="#profile" data-toggle="tab">Sản phẩm liên quan</a></li>
             
            </ul>
            <div id="myTabContent" class="tab-content tabWrapper">
            <div class="tab-pane fade active in" id="home">
			  ${product.details }
			</div>
			<div class="tab-pane fade" id="profile" name="profile">
			
			<hr class="soft">
			<div class="row-fluid">	  
			<div class="span2">
				<img src="assets/img/d.jpg" alt="">
			</div>
			<div class="span6">
				<h5>Product Name </h5>
				<p>
				Nowadays the lingerie industry is one of the most successful business spheres.
				We always stay in touch with the latest fashion tendencies - 
				that is why our goods are so popular..
				</p>
			</div>
			<div class="span4 alignR">
			<form class="form-horizontal qtyFrm">
			<h3> $140.00</h3>
			
			<div class="btn-group">
			  <a href="product_details.html" class="defaultBtn"><span class=" icon-shopping-cart"></span> Add to cart</a>
			  <a href="product_details.html" class="shopBtn">VIEW</a>
			 </div>
				</form>
			</div>
	</div>
			<hr class="soft"/>
			
			
			
			</div>
              	
              
            </div>

</div>
</div>
</div>
</body>
