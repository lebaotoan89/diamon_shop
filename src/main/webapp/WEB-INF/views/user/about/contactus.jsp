<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  
</head>
<body>
	<div class="container">
  <h2>Liên hệ : </h2>
  <p>Sản phẩm được cam kết với chất lượng và kiểu dáng tốt nhất .</p>
  <table class="table">
    <thead>
      <tr>
        <th>Thông tin :</th>
        <th>Chi tiết :</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Công ty</td>
        <td>Bảo Toàn's production</td>
      
      </tr>      
      <tr class="success">
        <td>Địa chỉ</td>
        <td>42/36E Ung Văn Khiêm, Bình Thành, TPHCM</td>
        
      </tr>
      <tr class="danger">
        <td>SĐT</td>
        <td>0909 766 334</td>
        
      </tr>
      <tr class="info">
        <td>Email</td>
        <td>lebaotoan89@gmail.com</td>
        
      </tr>
      <tr class="warning">
        <td>Phát triển bởi</td>
        <td>Lê Bảo Toàn Dev</td>
        
      </tr>
      <tr class="active">
        <td>Website</td>
        <td>diamondshop.com</td>
        
      </tr>
    </tbody>
  </table>
</div>
	
</body>
</html>