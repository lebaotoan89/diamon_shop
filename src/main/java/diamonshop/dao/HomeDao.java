package diamonshop.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import diamonshop.entity.MapperSlides;
import diamonshop.entity.Slides;

@Repository
public class HomeDao extends BaseDao{
	
	public List<Slides> getDataSlide(){
		List<Slides> list= new ArrayList<Slides>();
		String sql="SELECT * FROM `slides`";
		list=_jdbcTemplate.query(sql, new MapperSlides());
		return list;
	}
//	public static void main(String[] args) {
//		HomeDao dao= new HomeDao();
//		List<Slides> list= dao.getDataSlide();
//		
//		}
	
	
}
