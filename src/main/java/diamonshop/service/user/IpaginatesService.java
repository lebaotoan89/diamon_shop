package diamonshop.service.user;

import org.springframework.stereotype.Service;

import diamonshop.dto.PaginatesDto;

@Service
public interface IpaginatesService {
	public PaginatesDto getInfoPaginates(int totalPage, int limit, int currentPage);
}
