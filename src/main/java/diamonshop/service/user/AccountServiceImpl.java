package diamonshop.service.user;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import diamonshop.dao.UsersDao;
import diamonshop.entity.Users;
@Service
public class AccountServiceImpl implements IAccountService{
	@Autowired UsersDao usersDao = new UsersDao();
	public int addAccount(Users user) {
		user.setPassword(BCrypt.hashpw(user.getPassword(),BCrypt.gensalt(12)));
		return usersDao.addAccount(user);
	}
	public Users checkAccount(Users user) {
		String pass= user.getPassword();
		user = usersDao.getUserByAcc(user);
		if(user!=null) {
			if(BCrypt.checkpw(pass,user.getPassword())) {
				return user;
			} else {
				return null;
			}
			
		}
		return user;
	}

	

}
