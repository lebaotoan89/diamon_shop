package diamonshop.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import diamonshop.dao.ProductsDao;
import diamonshop.dto.ProductsDto;
@Service
public class ProductServiceImpl implements IProductService{
	@Autowired
	ProductsDao productsDao = new ProductsDao();
	
	public List<ProductsDto>  getProductByIDCategory(int id) {
		return productsDao.getAllProductsByID(id);
		
	}

	public ProductsDto getProductByID(long id) {
		List<ProductsDto> listProducts=productsDao.getProductByID(id);
		return listProducts.get(0);
	}
		

}
