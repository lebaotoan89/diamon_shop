package diamonshop.service.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import diamonshop.dao.CategorysDao;
import diamonshop.dao.HomeDao;
import diamonshop.dao.MenuDao;
import diamonshop.dao.ProductsDao;
import diamonshop.dto.ProductsDto;
import diamonshop.entity.Categorys;
import diamonshop.entity.MapperSlides;
import diamonshop.entity.Menus;
import diamonshop.entity.Slides;
@Service
public class HomeServiceImpl implements IHomeService{

//	@Autowired
//	private HomeDao homeDao;
//	public List<Slides> getDataSlide(){
//		return homeDao.getDataSlide();
//	}
//	
//	public Object getDataCategorys() {
//		// TODO Auto-generated method stub
//		return null;
//	}
	@Autowired
	private CategorysDao categorysDao;
	public List<Categorys> getDataCategorys() {
		// TODO Auto-generated method stub
		return categorysDao.getDataCategorys();
	}
	
	@Autowired
	private HomeDao homeDao;
	public List<Slides> getDataSlide() {
		// TODO Auto-generated method stub
		return homeDao.getDataSlide();
	}
	
	@Autowired
	private MenuDao menuDao;
	public List<Menus> getDataMenus() {
		// TODO Auto-generated method stub
		return menuDao.getDataMenus();
	}
	
	@Autowired
	private ProductsDao productsDao;
	public List<ProductsDto> getDataProducts() {
		List<ProductsDto> listProducts = productsDao.getDataProducts();
		return listProducts;
	}
	
	
	public List<ProductsDto> searchProductsByName(String name) {
		List<ProductsDto> listProductsSearcgByName= productsDao.searchProductsByName(name);
		return listProductsSearcgByName;
	}
	
}
