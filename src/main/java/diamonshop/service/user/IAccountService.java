package diamonshop.service.user;

import org.springframework.stereotype.Service;

import diamonshop.entity.Users;
@Service
public interface IAccountService {
	public int addAccount(Users user);
	public Users checkAccount(Users user);
}
