package diamonshop.service.user;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import diamonshop.dao.BillsDao;
import diamonshop.dto.CartDto;
import diamonshop.entity.BillDetail;
import diamonshop.entity.Bills;
@Service
public class BillsServiceImpl implements IBillsService{
	@Autowired private BillsDao billsDao;																																
	public int addBills(Bills bill) {
		return billsDao.addBills(bill);
	}

	public void addBillsDetail(HashMap<Long, CartDto> carts) {
		long idBills= billsDao.getIDLastBills();
		
		for(Map.Entry<Long, CartDto> itemCart: carts.entrySet()) {
			BillDetail billDetail = new BillDetail();
			billDetail.setId_bills(idBills);
			billDetail.setId_product(itemCart.getValue().getProduct().getId_product());
			billDetail.setQuanty(itemCart.getValue().getQuanty());
			billDetail.setTotal(itemCart.getValue().getTotalPrice());
		}		
		
	}

}
