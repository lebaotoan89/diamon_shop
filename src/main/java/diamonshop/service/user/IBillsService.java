package diamonshop.service.user;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import diamonshop.dto.CartDto;
import diamonshop.entity.Bills;
@Service 	
public interface IBillsService {
	public int addBills(Bills bill);
	public void addBillsDetail(HashMap<Long,CartDto>carts);
}
