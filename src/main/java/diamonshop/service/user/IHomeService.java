package diamonshop.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import diamonshop.dto.ProductsDto;
import diamonshop.entity.Categorys;
import diamonshop.entity.Menus;
import diamonshop.entity.Slides;


public interface IHomeService {
	@Autowired
	public List<Slides> getDataSlide();
	public List<Categorys> getDataCategorys();
	public List<Menus> getDataMenus();
	public List<ProductsDto> getDataProducts();

}