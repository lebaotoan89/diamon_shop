package diamonshop.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperCategorys implements RowMapper <Categorys>{
	public Categorys mapRow(ResultSet rs, int rowNum) {
		Categorys categorys= new Categorys();
		try {
			categorys.setId(rs.getInt("id"));
			categorys.setName(rs.getString("name"));
			categorys.setDescription(rs.getString("description"));
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return categorys;
	}
}