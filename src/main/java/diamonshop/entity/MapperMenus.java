package diamonshop.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperMenus implements RowMapper<Menus>{
	public Menus mapRow(ResultSet rs1, int rowNum) {
		Menus menus= new Menus();
		try {
			menus.setId(rs1.getInt("id"));
			menus.setName(rs1.getString("name"));
			menus.setUrl(rs1.getString("url"));
	
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return menus;
	}
}
