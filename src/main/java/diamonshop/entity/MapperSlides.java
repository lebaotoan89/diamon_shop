package diamonshop.entity;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperSlides implements RowMapper<Slides> {
	public Slides mapRow(ResultSet rs, int rowNum) {
		Slides slides = new Slides();
		try {
			slides.setId(rs.getInt("id"));
			slides.setImg(rs.getString("img"));
			slides.setCaption(rs.getString("caption"));
			slides.setContent(rs.getString("content"));

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return slides;
	}
}
