package diamonshop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;
import diamonshop.entity.Users;

@Controller
public class HomeController extends BaseController {
	
	@RequestMapping (value={"/","/trang-chu"}, method = RequestMethod.GET)
	public ModelAndView Index(){
//		ModelAndView mv = new ModelAndView("user/index");
		_mvShare.addObject("slides",_homeService.getDataSlide());
		_mvShare.addObject("categorys",_homeService.getDataCategorys());
		_mvShare.addObject("products",_homeService.getDataProducts() );
		
		_mvShare.addObject("productsDto",new ProductsDto());
		
		
		_mvShare.setViewName("user/index");
		return _mvShare;
	}
	@RequestMapping (value="/product", method = RequestMethod.GET)
	public ModelAndView Product(){
		ModelAndView mv = new ModelAndView("user/product");
		return mv;
	}
}
