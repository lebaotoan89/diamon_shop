package diamonshop.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;
import diamonshop.service.user.IProductService;

@Controller
public class ProductController extends BaseController{
	@Autowired IProductService _productService;
	@RequestMapping (value={"chi-tiet-san-pham/{id}"}, method = RequestMethod.GET)
	public ModelAndView Index(@PathVariable long id){
		_mvShare.addObject("productsDto",new ProductsDto());
		_mvShare.setViewName("user/products/product");
		_mvShare.addObject("categorys", _homeService.getDataCategorys());
		
		_mvShare.addObject("product",_productService.getProductByID(id));
		int idCategory= _productService.getProductByID(id).getId_category();
		_mvShare.addObject("productByIDCategory",_productService.getProductByIDCategory(idCategory));
		return _mvShare;
	}
}
