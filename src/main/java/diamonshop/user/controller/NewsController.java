package diamonshop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;
@Controller
public class NewsController extends BaseController {
	@RequestMapping (value={"/bai-viet"}, method = RequestMethod.GET)
	public ModelAndView compare(){
//		ModelAndView mv = new ModelAndView("user/index");
		_mvShare.addObject("slides",_homeService.getDataSlide());
		_mvShare.addObject("categorys",_homeService.getDataCategorys());
		_mvShare.addObject("products",_homeService.getDataProducts() );
		
		_mvShare.addObject("productsDto",new ProductsDto());
		
		
		_mvShare.setViewName("user/news/newsView");
		return _mvShare;
	}
}
