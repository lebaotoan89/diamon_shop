package diamonshop.user.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.CartDto;
import diamonshop.dto.ProductsDto;
import diamonshop.entity.Bills;
import diamonshop.entity.Users;
import diamonshop.service.user.BillsServiceImpl;
import diamonshop.service.user.CartServiceImpl;
import diamonshop.service.user.IProductService;

@Controller
public class CartController extends BaseController{

	@Autowired private CartServiceImpl cartService= new CartServiceImpl();
	@Autowired private BillsServiceImpl billsService;
	@RequestMapping(value= "gio-hang")
	public ModelAndView Index(){
//		ModelAndView mv = new ModelAndView("user/index");
		_mvShare.addObject("productsDto",new ProductsDto());
		
		_mvShare.addObject("slides",_homeService.getDataSlide());
		_mvShare.addObject("categorys",_homeService.getDataCategorys());
		_mvShare.addObject("products",_homeService.getDataProducts() );
		_mvShare.setViewName("user/cart/list_cart");
		return _mvShare;
	}
	@RequestMapping(value = "AddCart/{id}")
	public String addCart(HttpServletRequest request, HttpSession session, @PathVariable long id) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>) session.getAttribute("Cart");
		if(cart == null) {
			cart= new HashMap<Long,CartDto>();
		}

		cart= cartService.addCart(id, cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuantyCart", cartService.totalQuanty(cart));
		session.setAttribute("TotalPriceCart", cartService.totalPrice(cart));
//		return "redirect:/chi-tiet-san-pham/" + id;
		return "redirect:"+request.getHeader("Referer");	
	}
	@RequestMapping(value= "EditCart/{id}/{quanty}")
	public String editCart(HttpServletRequest request, HttpSession session,@PathVariable long id, @PathVariable int quanty) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>) session.getAttribute("Cart");
		if(cart == null) {
			cart= new HashMap<Long,CartDto>();
	}

		cart= cartService.editCart(id, quanty,cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuantyCart", cartService.totalQuanty(cart));
		session.setAttribute("TotalPriceCart", cartService.totalPrice(cart));
//		return "redirect:/chi-tiet-san-pham/" + id;
		return "redirect:"+request.getHeader("Referer");	
	}
	@RequestMapping(value = "DeleteCart/{id}")
	public String deleteCart(HttpServletRequest request, HttpSession session, @PathVariable long id) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>) session.getAttribute("Cart");
		if(cart == null) {
			cart= new HashMap<Long,CartDto>();
		}
		cart= cartService.deleteCart(id, cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuantyCart", cartService.totalQuanty(cart));
		session.setAttribute("TotalPriceCart", cartService.totalPrice(cart));
		return "redirect:"+request.getHeader("Referer");	
	}
	@RequestMapping(value = "checkout", method=RequestMethod.GET)
	public ModelAndView CheckOut(HttpServletRequest request, HttpSession session) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		_mvShare.setViewName("user/bills/checkout");
		Bills bills = new Bills();
		Users LoginInfo = (Users) session.getAttribute("LoginInfo");
		if(LoginInfo!=null) {
			bills.setAddress(LoginInfo.getAddress());
			bills.setDisplay_name(LoginInfo.getDisplay_name());
			bills.setUser(LoginInfo.getUser());	
		
		_mvShare.addObject("bills",bills);
		return _mvShare;
		}
		_mvShare.addObject("user",new Users());
		_mvShare.addObject("productsDto",new ProductsDto());
		_mvShare.setViewName("user/account/register");
		
		return _mvShare;
	}
	@RequestMapping(value="checkout", method = RequestMethod.POST)
	public String checkOutBill(HttpServletRequest request , HttpSession session, @ModelAttribute ("bills") Bills bill) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		bill.setQuanty(Integer.parseInt(session.getAttribute("TotalQuantyCart").toString()));
		bill.setTotal(Double.parseDouble( session.getAttribute("TotalPriceCart").toString()));
		if(billsService.addBills(bill)>0) {
			HashMap<Long, CartDto> carts = (HashMap<Long, CartDto>) session.getAttribute("Cart");
			billsService.addBillsDetail(carts);
		}
		session.removeAttribute("Cart");
		return "redirect:gio-hang";
	}	
}																				
