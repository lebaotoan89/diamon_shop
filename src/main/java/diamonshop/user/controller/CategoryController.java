package diamonshop.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.PaginatesDto;
import diamonshop.dto.ProductsDto;
import diamonshop.service.user.CategoryServiceImpl;
import diamonshop.service.user.PaginateServiceImpl;

@Controller
public class CategoryController extends BaseController{
	@Autowired CategoryServiceImpl categoryServiceImpl;	
	@Autowired PaginateServiceImpl paginateServiceImpl;
	private int totalProductsInPage = 9;
	@RequestMapping(value = "/san-pham/{id}")
	
	public  ModelAndView Product(@PathVariable String id) {
		_mvShare.addObject("productsDto",new ProductsDto());
		_mvShare.setViewName("user/products/category");
		
		
		
		int totalData= categoryServiceImpl.getAllProductsByID(Integer.parseInt(id)).size();
		PaginatesDto paginatesInfo	 = paginateServiceImpl.getInfoPaginates(totalData, totalProductsInPage, 1);
		
		_mvShare.addObject("idCategory",id);
		_mvShare.addObject("paginatesInfo",paginatesInfo);
		
		_mvShare.addObject("ProductsPaginate",categoryServiceImpl.getDataProductPaginate(Integer.parseInt(id),paginatesInfo.getStart(), totalProductsInPage));
		return _mvShare;
	}
	
	@RequestMapping(value = "/san-pham/{id}/{currentPage}")
	public  ModelAndView Product1(@PathVariable String id, @PathVariable String currentPage) {
		_mvShare.addObject("productsDto",new ProductsDto());
		_mvShare.setViewName("user/products/category");
		
		
		
		int totalData= categoryServiceImpl.getAllProductsByID(Integer.parseInt(id)).size();
		PaginatesDto paginatesInfo	 = paginateServiceImpl.getInfoPaginates(totalData, totalProductsInPage,Integer.parseInt(currentPage));
		_mvShare.addObject("idCategory",id);
		_mvShare.addObject("paginatesInfo",paginatesInfo);
		
		_mvShare.addObject("ProductsPaginate",categoryServiceImpl.getDataProductPaginate(Integer.parseInt(id),paginatesInfo.getStart(), totalProductsInPage));
		return _mvShare;
	}
	
	
}
