package diamonshop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;

@Controller
public class ContactController extends BaseController {
	@RequestMapping (value={"/lien-he"}, method = RequestMethod.GET)
	public ModelAndView Index1(){
		_mvShare.setViewName("user/about/contactus");
		_mvShare.addObject("productsDto",new ProductsDto());
		return _mvShare;
	}
}
