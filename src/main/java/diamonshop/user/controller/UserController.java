package diamonshop.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;
import diamonshop.entity.Users;
import diamonshop.service.user.AccountServiceImpl;

@Controller
public class UserController extends BaseController{
	@Autowired AccountServiceImpl accountservice = new AccountServiceImpl();
	
	@RequestMapping(value ="/dang-ky", method=RequestMethod.GET)
	public ModelAndView register(){
	_mvShare.addObject("productsDto",new ProductsDto());
	_mvShare.addObject("categorys", _homeService.getDataCategorys());
		
	_mvShare.setViewName("user/account/register");
	
		
		
		
		return _mvShare;
	}
	@RequestMapping(value ="/dang-ky", method=RequestMethod.POST)
	public ModelAndView CreateAcc (@ModelAttribute ("user") Users user){
		_mvShare.addObject("productsDto",new ProductsDto());
		_mvShare.addObject("categorys", _homeService.getDataCategorys());
		
		
		int  count = accountservice.addAccount(user);
		if(count>0) {
			_mvShare.addObject("status","Đăng ký tài khoản thành công");
		} else 	_mvShare.addObject("status","Đăng ký tài khoản thất bại");
		
		
		_mvShare.setViewName("user/account/register");
		return _mvShare;
	}
	@RequestMapping(value ="/dang-nhap", method=RequestMethod.POST)
	public ModelAndView Login (HttpSession session, @ModelAttribute ("user") Users user){
		_mvShare.addObject("productsDto",new ProductsDto());
		
		 user= accountservice.checkAccount(user);
		if(user!=null) {	
			_mvShare.setViewName("redirect:trang-chu");
			session.setAttribute("LoginInfo", user);
		} else 	{
			_mvShare.addObject("StatusLogin","Đăng nhập thất bại");
		}
		return _mvShare;
	}
	
	@RequestMapping(value="/dang-xuat",method = RequestMethod.GET)
	public String Logout(HttpSession session,HttpServletRequest request) {
		_mvShare.addObject("productsDto",new ProductsDto());
		
		session.removeAttribute("Cart");
		session.removeAttribute("TotalQuantyCart");
		session.removeAttribute("TotalPriceCart");
		
		session.removeAttribute("LoginInfo");
		return "redirect:"+request.getHeader("Referer");
	}

}