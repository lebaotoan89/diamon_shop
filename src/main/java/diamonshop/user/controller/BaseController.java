package diamonshop.user.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.service.user.HomeServiceImpl;
@Controller
public class BaseController {
	@Autowired HomeServiceImpl _homeService;
	public ModelAndView _mvShare= new ModelAndView();
	
	@PostConstruct
	public ModelAndView Init() {
		_mvShare.addObject("menus",_homeService	.getDataMenus());
		return _mvShare;	
	}
}
