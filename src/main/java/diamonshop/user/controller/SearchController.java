package diamonshop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import diamonshop.dto.ProductsDto;

@Controller
public class SearchController extends BaseController{
//	@ModelAttribute("productsDto")
//	public ProductsDto setProductsDto () {
//		return new ProductsDto() {
//		};
//	}
	
	
	@RequestMapping (value={"/search"}, method = RequestMethod.POST)
	public ModelAndView Index1(@ModelAttribute ("productsDto") ProductsDto productsDto,Model model){
//		ModelAndView mv = new ModelAndView("user/index");
		
		
		
//		model.addAttribute("productDto", new ProductsDto(productsDto.getName()) {
//		});
		_mvShare.addObject("slides",_homeService.getDataSlide());
		_mvShare.addObject("categorys",_homeService.getDataCategorys());
		_mvShare.addObject("products",_homeService.searchProductsByName(productsDto.getName()) );
		_mvShare.setViewName("user/index");
		return _mvShare;
	}
}
